/**
 * RankedPlaylistCache.java
 */
package com.sk.demos.playlists.app.svc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import com.sk.demos.playlists.app.exceptions.PLMessages;
import com.sk.demos.playlists.app.exceptions.PlException;
import com.sk.demos.playlists.app.exceptions.PlRuntimeException;
import com.sk.demos.playlists.app.storage.ConnectionManager;
import com.sk.demos.playlists.app.storage.SimpleCache;
import com.sk.demos.playlists.app.types.Playlist;
import com.sk.demos.playlists.app.types.RankedPlaylist;
import com.sk.demos.playlists.app.types.RankedPlaylistKey;
import com.sk.demos.playlists.app.types.TagList;
import com.sk.demos.playlists.app.utils.SerDeUtils;

/**
 * @author sanjay
 *
 */
public class RankedPlaylistCache extends SimpleCache<RankedPlaylistKey, RankedPlaylist>
{
  private static final String STMT_LOAD =
      "select * from (select name, id, plays, likes, tag_list "
          + "         from playlists, tagindices "
          + "         where (id = playlist_id) "
          + "         and (tag = ?) "
          + "         order by plays, likes)"
          + " where (rownum <= ?)";

  private static final int TARGET_SIZE = 1000;

  private static RankedPlaylistCache INSTANCE = new RankedPlaylistCache();

  private RankedPlaylistCache()
  {
    super(TARGET_SIZE);
  }

  public static RankedPlaylistCache getInstance()
  {
    return INSTANCE;
  }

  protected RankedPlaylist load(RankedPlaylistKey key) throws PlException
  {
    RankedPlaylist value = new RankedPlaylist();

    ConnectionManager m = new ConnectionManager();
    Connection c = m.getConnection();

    try
    {
      PreparedStatement stmt = c.prepareStatement(STMT_LOAD);
      stmt.setString(1, key.getTag());
      stmt.setLong(2, key.getEndPosition());
      ResultSet rs = stmt.executeQuery();
      long rowNum = 0;
      while (rs.next())
      {
        if (++rowNum >= key.getStartPosition())
        {
          Playlist o = new Playlist();
          o.setId(rs.getLong("id"));
          o.setName(rs.getString("name"));
          o.setLikes(rs.getLong("likes"));
          o.setPlays(rs.getLong("plays"));
          o.setTags((TagList) SerDeUtils.deSerializeUsingJaxb(rs.getString(
              "tag_list")));
          value.addPlaylist(o);
        }
      }
      stmt.close();
    }
    catch (Exception e)
    {
      value = null;
    }
    finally
    {
      try
      {
        c.close();
      }
      catch (SQLException e)
      {
        throw new PlRuntimeException(PLMessages.PL_DB_OPERATION, e);
      }
    }

    return value;
  }
}
