/**
 * Config.java
 */
package com.sk.demos.playlists.app.types;

import java.io.File;

import org.apache.commons.configuration2.Configuration;
import org.apache.commons.configuration2.builder.fluent.Configurations;
import org.apache.commons.configuration2.ex.ConfigurationException;

import com.sk.demos.playlists.app.exceptions.PLMessages;
import com.sk.demos.playlists.app.exceptions.PlException;
import lombok.Getter;
import lombok.Setter;

/**
 * @author sanjay
 *
 */
@Getter
@Setter
public class Config
{
  public static String DB_TYPE;

  public static String DB_HOST;

  public static String DB_PORT;

  public static String DB_SID;
  
  public static String DB_USER;
  
  public static String DB_PASSWORD;

  public static void initFromFile(String fileName) throws PlException
  {
    Configurations configs = new Configurations();
    try
    {
      Configuration config = configs.properties(new File(fileName));
      DB_TYPE = (String) config.getString(ConfigProperties.DB_TYPE
          .value());
      DB_HOST = (String) config.getString(ConfigProperties.DB_HOST
          .value());
      DB_PORT = (String) config.getString(ConfigProperties.DB_PORT
          .value());
      DB_SID = (String) config.getString(ConfigProperties.DB_SID
          .value());
      DB_USER = (String) config.getString(ConfigProperties.DB_USER
          .value());
      DB_PASSWORD = (String) config.getString(ConfigProperties.DB_PASSWORD
          .value());
    }
    catch (ConfigurationException e)
    {
      throw new PlException(PLMessages.PL_INTERNAL_ERROR, e);
    }
  }
}
