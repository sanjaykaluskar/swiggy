package com.sk.demos.playlists.app.storage;

public enum StorageType
{
  ORACLE;

  public String value()
  {
    return name();
  }

  public static StorageType fromValue(String v)
  {
    return valueOf(v);
  }
  
  public static String[] stringvalues()
  {
    String[] ret = new String[StorageType.values().length];
    
    int i = 0;
    for (StorageType s : StorageType.values())
      ret[i++] = s.value();
    
    return ret;
  }
}
