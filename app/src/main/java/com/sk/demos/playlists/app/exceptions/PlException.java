/**
 * PlException.java
 */
package com.sk.demos.playlists.app.exceptions;

import com.sk.demos.playlists.app.interfaces.IPlMessage;

/**
 * @author sanjay
 * 
 */
public class PlException extends Exception
{
  /** PlException.java */
  private static final long serialVersionUID = -3512796491192594963L;

  private IPlMessage message;

  public IPlMessage getMessageEnum()
  {
    return message;
  }

  public PlException(PLMessages m)
  {
    super(m.getMessage());
    message = m;
  }

  public PlException(PLMessages m, String... arg)
  {
    super(m.getMessage());
    message = m;
  }

  public PlException(PLMessages m, Exception e)
  {
    super(m.getMessage());
    message = m;
  }

  public PlException(PLMessages m, Exception e, String... strings)
  {
    super(m.getMessage());
    message = m;
  }
}
