/**
 * PlaylistMgmtFactory.java
 */
package com.sk.demos.playlists.app.svc;

import com.sk.demos.playlists.app.exceptions.PlException;
import com.sk.demos.playlists.app.interfaces.IPlaylistMgmt;

/**
 * @author sanjay
 *
 */
public class PlaylistMgmtFactory
{
  private static PlaylistMgmtService svc;

  public static synchronized IPlaylistMgmt getService() throws PlException
  {
    if (svc == null)
      svc = new PlaylistMgmtService();
    
    return svc;
  }
}
