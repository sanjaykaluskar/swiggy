/**
 * ConnectionManager.java
 */
package com.sk.demos.playlists.app.storage;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import com.sk.demos.playlists.app.exceptions.PLMessages;
import com.sk.demos.playlists.app.exceptions.PlException;
import com.sk.demos.playlists.app.exceptions.PlRuntimeException;
import com.sk.demos.playlists.app.types.Config;

import lombok.AllArgsConstructor;

/**
 * @author sanjay
 *
 */
@AllArgsConstructor
public class ConnectionManager
{
  private static final String ORACLE_JDBC_URL_FORMAT =
      "jdbc:oracle:thin:@//%s:%s/%s";

  /** URL to access the database */
  private String dbUrl;
  private String user;
  private String password;

  public ConnectionManager()
  {
    user = Config.DB_USER;
    password = Config.DB_PASSWORD;
    initUrl(StorageType.fromValue(Config.DB_TYPE), Config.DB_HOST,
        Config.DB_PORT, Config.DB_SID);
  }

  private void registerDriver(StorageType sType)
      throws ClassNotFoundException
  {
    switch (sType)
    {
    case ORACLE:
      Class.forName("oracle.jdbc.driver.OracleDriver");
      break;
    default:
      break;
    }
  }

  private void initUrl(StorageType sType, String host, String port, String sid)
  {
    try
    {
      registerDriver(sType);
      switch (sType)
      {
      case ORACLE:
        dbUrl = String.format(ORACLE_JDBC_URL_FORMAT, host, port, sid);
        break;
      default:
        break;
      }
    }
    catch (Exception e)
    {
      throw new PlRuntimeException(PLMessages.PL_DB_OPERATION, e);
    }
  }

  public Connection getConnection() throws PlException
  {
    Connection cxn = null;

    try
    {
      cxn = DriverManager.getConnection(dbUrl, user, password);
    }
    catch (SQLException e)
    {
      throw new PlException(PLMessages.PL_DB_OPERATION, e);
    }

    return cxn;
  }
}
