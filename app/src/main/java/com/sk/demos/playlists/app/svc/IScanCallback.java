/**
 * IScanCallback.java
 */
package com.sk.demos.playlists.app.svc;

import com.sk.demos.playlists.app.types.RankedPlaylistKey;

/**
 * @author sanjay
 *
 */
public interface IScanCallback
{
  public boolean removeKey(RankedPlaylistKey key);
}
