/**
 * TagCount.java
 */
package com.sk.demos.playlists.app.types;

import lombok.Getter;
import lombok.Setter;

/**
 * @author sanjay
 *
 */
@Getter
@Setter
public class TagCount
{
  private String tag;
  
  private long count;
}
