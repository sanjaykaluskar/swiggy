/**
 * StorageResultSet.java
 */
package com.sk.demos.playlists.app.storage;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.sk.demos.playlists.app.exceptions.PLMessages;
import com.sk.demos.playlists.app.exceptions.PlException;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author sanjay
 *
 *         Abstracts a result set of type ObjectType from query execution.
 */
@AllArgsConstructor
@Getter
public abstract class StorageResultSet<ObjectType>
{
  protected ResultSet  rs;
  protected Statement  stmt;
  protected Connection cxn;

  /**
   * Returns next row from the result set.
   * 
   * @return next row if it exists else null
   */
  public abstract ObjectType next() throws PlException;

  /**
   * closes the result set and any associated resources. This must be called for
   * every result set.
   */
  public void close() throws PlException
  {
    try
    {
      rs.close();
      stmt.close();
      cxn.close();
    }
    catch (SQLException e)
    {
      try
      {
        if ((cxn != null) && !cxn.isClosed())
          cxn.close();
      }
      catch (SQLException e1)
      {
        throw new PlException(PLMessages.PL_DB_OPERATION, e1);
      }
      throw new PlException(PLMessages.PL_DB_OPERATION, e);
    }
  }
}
