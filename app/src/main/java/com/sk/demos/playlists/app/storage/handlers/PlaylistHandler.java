/**
 * PlaylistHandler.java
 */
package com.sk.demos.playlists.app.storage.handlers;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.sk.demos.playlists.app.exceptions.PLMessages;
import com.sk.demos.playlists.app.exceptions.PlException;
import com.sk.demos.playlists.app.storage.RecordHandler;
import com.sk.demos.playlists.app.storage.StorageResultSet;
import com.sk.demos.playlists.app.types.Playlist;
import com.sk.demos.playlists.app.types.TagList;
import com.sk.demos.playlists.app.utils.SerDeUtils;

/**
 * @author sanjay
 * 
 */
public class PlaylistHandler extends RecordHandler<Long, Playlist>
{
  public class PlaylistResultSet extends StorageResultSet<Playlist>
  {
    public PlaylistResultSet(ResultSet rs, Statement stmt, Connection cxn)
    {
      super(rs, stmt, cxn);
    }

    @Override
    public Playlist next() throws PlException
    {
      try
      {
        Playlist o = null;

        if (rs.next())
        {
          o = new Playlist();
          o.setId(rs.getLong("id"));
          o.setName(rs.getString("name"));
          o.setLikes(rs.getLong("likes"));
          o.setPlays(rs.getLong("plays"));
          o.setTags((TagList) SerDeUtils.deSerializeUsingJaxb(rs.getString(
              "tag_list")));
        }

        return o;
      }
      catch (Exception e)
      {
        try
        {
          if ((cxn != null) && !cxn.isClosed())
            cxn.close();
        }
        catch (SQLException e1)
        {
          throw new PlException(PLMessages.PL_DB_OPERATION, e1);
        }
        throw new PlException(PLMessages.PL_DB_OPERATION, e);
      }
    }
  }

  private static final String STMT_CREATE_TABLE = "create table playlists ("
      + "id number primary key, "
      + "name varchar2(4000), "
      + "likes number, "
      + "plays number, "
      + "tag_list varchar2(4000))";

  private static final String STMT_DROP = "drop table playlists";

  private static final String STMT_SELECT = "select "
      + "id, name, likes, plays, tag_list "
      + "from playlists "
      + "where id = ?";

  private static final String STMT_INSERT = "insert into playlists"
      + "(id, name, likes, plays, tag_list) "
      + "values(?, ?, ?, ?, ?)";

  private static final String STMT_UPDATE = "update playlists set "
      + "    name = ?, "
      + "    likes = ?, "
      + "    plays = ?, "
      + "    tag_list = ? "
      + "where (id = ?)";

  private static final String STMT_DELETE = "delete from playlists "
      + "where (id = ?)";

  private static final String STMT_SEARCH = "select "
      + "id, name, likes, plays, tag_list "
      + "from playlists "
      + "where (%s)";

  public PlaylistHandler()
  {
    super(Playlist.class);
  }

  @Override
  public void createContainer(Connection c)
      throws PlException
  {
    try
    {
      Statement stmt = c.createStatement();
      stmt.execute(STMT_CREATE_TABLE);
      stmt.close();
    }
    catch (Exception e)
    {
      throw new PlException(PLMessages.PL_DB_OPERATION, e);
    }
  }

  @Override
  public void deleteContainer(Connection c)
      throws PlException
  {
    try
    {
      Statement stmt = c.createStatement();
      stmt.execute(STMT_DROP);
      stmt.close();
    }
    catch (Exception e)
    {
      throw new PlException(PLMessages.PL_DB_OPERATION, e);
    }
  }

  @Override
  public void create(Connection c, Playlist o)
      throws PlException
  {
    try
    {
      PreparedStatement stmt = c.prepareStatement(STMT_INSERT);
      stmt.setLong(1, o.getId());
      stmt.setString(2, o.getName());
      stmt.setLong(3, o.getLikes());
      stmt.setLong(4, o.getPlays());
      stmt.setString(5, SerDeUtils.serializeUsingJaxb(o.getTags(),
          Playlist.class));
      stmt.executeUpdate();
      stmt.close();
    }
    catch (Exception e)
    {
      throw new PlException(PLMessages.PL_DB_OPERATION, e);
    }
  }

  @Override
  public Playlist read(Connection c, Long key) throws PlException
  {
    long id = key.longValue();
    Playlist o = null;

    try
    {
      PreparedStatement stmt = c.prepareStatement(STMT_SELECT);
      stmt.setLong(1, id);
      ResultSet rs = stmt.executeQuery();
      if (rs.next())
      {
        o = new Playlist();
        o.setId(rs.getLong("id"));
        o.setName(rs.getString("name"));
        o.setLikes(rs.getLong("likes"));
        o.setPlays(rs.getLong("plays"));
        o.setTags((TagList) SerDeUtils.deSerializeUsingJaxb(rs.getString(
            "tag_list")));
        stmt.close();
      }
    }
    catch (Exception e)
    {
      o = null;
    }
    return o;
  }

  @Override
  public void update(Connection c, Playlist o) throws PlException
  {
    try
    {
      PreparedStatement stmt = c.prepareStatement(STMT_UPDATE);
      stmt.setString(1, o.getName());
      stmt.setLong(2, o.getLikes());
      stmt.setLong(3, o.getPlays());
      stmt.setString(4, SerDeUtils.serializeUsingJaxb(o.getTags(),
          Playlist.class));
      stmt.setLong(5, o.getId());
      stmt.executeUpdate();
      stmt.close();
    }
    catch (Exception e)
    {
      throw new PlException(PLMessages.PL_DB_OPERATION, e);
    }
  }

  @Override
  public void delete(Connection c, Playlist o) throws PlException
  {
    try
    {
      PreparedStatement stmt = c.prepareStatement(STMT_DELETE);
      stmt.setLong(1, o.getId());
      stmt.executeUpdate();
      stmt.close();
    }
    catch (Exception e)
    {
      throw new PlException(PLMessages.PL_DB_OPERATION, e);
    }
  }

  @Override
  public PlaylistResultSet search(Connection c, String condition)
      throws PlException
  {
    PlaylistResultSet rs = null;

    try
    {
      String sql = String.format(STMT_SEARCH, condition);
      PreparedStatement stmt = c.prepareStatement(sql);
      ResultSet resultSet = stmt.executeQuery();
      if (resultSet != null)
      {
        rs = new PlaylistResultSet(resultSet, stmt, c);
      }
      return rs;
    }
    catch (Exception e)
    {
      throw new PlException(PLMessages.PL_DB_OPERATION, e);
    }
  }
}
