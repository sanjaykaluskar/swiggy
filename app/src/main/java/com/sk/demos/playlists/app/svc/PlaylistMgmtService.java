/**
 * PlaylistMgmtService.java
 */
package com.sk.demos.playlists.app.svc;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sk.demos.playlists.app.exceptions.PlException;
import com.sk.demos.playlists.app.interfaces.IPlaylistMgmt;
import com.sk.demos.playlists.app.storage.ICacheCallback;
import com.sk.demos.playlists.app.storage.IStorageMgmt;
import com.sk.demos.playlists.app.storage.StorageMgmtService;
import com.sk.demos.playlists.app.storage.StorageTransaction;
import com.sk.demos.playlists.app.storage.handlers.PlaylistHandler.PlaylistResultSet;
import com.sk.demos.playlists.app.types.Playlist;
import com.sk.demos.playlists.app.types.RankedPlaylist;
import com.sk.demos.playlists.app.types.RankedPlaylistKey;
import com.sk.demos.playlists.app.types.TagCount;
import com.sk.demos.playlists.app.types.TagIndex;
import com.sk.demos.playlists.app.types.TagList;
import com.sk.demos.playlists.app.utils.SerDeUtils;
import com.sk.demos.playlists.app.utils.StringUtils;

/**
 * @author sanjay
 *
 *         This class is intentionally not public. It is only instantiated
 *         through the factory. External clients can only invoke methods
 *         declared in the interface.
 */
class PlaylistMgmtService implements IPlaylistMgmt
{
  private static long NEXT_ID = 1;

  private static final String COND_MAX_ID =
      "id >= (select max(id) from playlists)";

  private static Logger logger = LoggerFactory.getLogger(
      PlaylistMgmtService.class);

  private IStorageMgmt storageSvc = StorageMgmtService.getService();

  private RankedPlaylistCache rpCache = RankedPlaylistCache.getInstance();

  private TagCountCache tcCache = TagCountCache.getCache();

  PlaylistMgmtService() throws PlException
  {
    StorageTransaction txn = storageSvc.beginTxn();
    PlaylistResultSet rs = null;
    try
    {
      rs = (PlaylistResultSet) storageSvc.search(txn,
          COND_MAX_ID, Playlist.class);
      if (rs != null)
      {
        Playlist p = rs.next();
        if (p != null)
          NEXT_ID = p.getId() + 1;
        rs.close();
      }
      storageSvc.commitTxn(txn);
      logger.debug("Set NEXT_ID={}", String.valueOf(NEXT_ID));
    }
    catch (Exception e)
    {
      if (rs != null)
        rs.close();
      storageSvc.abortTxn(txn);
    }
  }

  /*
   * (non-Javadoc)
   * 
   * @see
   * com.sk.demos.playlists.app.interfaces.IPlaylistMgmt#create(com.sk.demos.
   * playlists.app.types.Playlist)
   */
  @Override
  public long create(Playlist o) throws PlException
  {
    o.setId(NEXT_ID++);
    logger.debug("Creating playlist - id: {}", o.getId());
    StorageTransaction txn = storageSvc.beginTxn();
    try
    {
      storageSvc.create(txn, o);
      addIndexEntries(txn, o);
      incrTagCounts(txn, o.getTags().getTags());
      storageSvc.commitTxn(txn);

      /* purge playlist cache */
      purgeCachedPlaylists(o.getTags().getTags());

      /* purge tag counts */
      purgeCachedCounts(o.getTags().getTags());
    }
    catch (Exception e)
    {
      storageSvc.abortTxn(txn);
      throw (e);
    }
    return o.getId();
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.sk.demos.playlists.app.interfaces.IPlaylistMgmt#read(long)
   */
  @Override
  public Playlist read(long id) throws PlException
  {
    logger.debug("Reading playlist - id: {}", String.valueOf(id));
    StorageTransaction txn = storageSvc.beginTxn();
    Playlist o;
    try
    {
      o = (Playlist) storageSvc.read(txn, id, Playlist.class);
      storageSvc.commitTxn(txn);
    }
    catch (Exception e)
    {
      storageSvc.abortTxn(txn);
      throw (e);
    }
    return o;
  }

  /*
   * (non-Javadoc)
   * 
   * @see
   * com.sk.demos.playlists.app.interfaces.IPlaylistMgmt#update(com.sk.demos.
   * playlists.app.types.Playlist)
   */
  @Override
  public void update(Playlist o) throws PlException
  {
    logger.debug("Updating playlist - id: {}", o.getId());
    StorageTransaction txn = storageSvc.beginTxn();
    try
    {
      Playlist oldPlaylist = (Playlist) storageSvc.read(txn, o.getId(),
          Playlist.class);
      storageSvc.update(txn, o);
      removeIndexEntries(txn, oldPlaylist);
      addIndexEntries(txn, o);
      decrTagCounts(txn, oldPlaylist.getTags().getTags());
      incrTagCounts(txn, o.getTags().getTags());
      storageSvc.commitTxn(txn);

      /*
       * Purge playlist cache for all the tags (old & new). TODO: One can be
       * more selective about this.
       */
      purgeCachedPlaylists(oldPlaylist.getTags().getTags());
      purgeCachedPlaylists(o.getTags().getTags());

      /* purge tag counts */
      purgeCachedCounts(oldPlaylist.getTags().getTags());
      purgeCachedCounts(o.getTags().getTags());
    }
    catch (Exception e)
    {
      storageSvc.abortTxn(txn);
      throw (e);
    }
  }

  /*
   * (non-Javadoc)
   * 
   * @see
   * com.sk.demos.playlists.app.interfaces.IPlaylistMgmt#delete(com.sk.demos.
   * playlists.app.types.Playlist)
   */
  @Override
  public void delete(Playlist o) throws PlException
  {
    logger.debug("Deleting playlist - id: {}", o.getId());
    StorageTransaction txn = storageSvc.beginTxn();
    try
    {
      storageSvc.delete(txn, o);
      removeIndexEntries(txn, o);
      decrTagCounts(txn, o.getTags().getTags());
      storageSvc.commitTxn(txn);

      /* purge playlist cache */
      purgeCachedPlaylists(o.getTags().getTags());

      /* purge tag counts */
      purgeCachedCounts(o.getTags().getTags());
    }
    catch (Exception e)
    {
      storageSvc.abortTxn(txn);
    }
  }

  /*
   * (non-Javadoc)
   * 
   * @see
   * com.sk.demos.playlists.app.interfaces.IPlaylistMgmt#lookupByTags(java.util.
   * List, java.util.List, int, int)
   */
  @Override
  public List<Playlist> lookupByTags(List<String> tags, List<String> extraTags,
      int start, int limit) throws PlException
  {
    logger.debug("Looking up by tags: {}", StringUtils.concatenate(tags, StringUtils.STR_COMMA));
    String bestTag = mostSelectiveTag(tags);

    /* lookup ranked playlists using best tag */
    RankedPlaylistKey key = new RankedPlaylistKey();
    key.setTag(bestTag);
    key.setStartPosition(start);
    key.setEndPosition(start + limit - 1);
    RankedPlaylist rpList = rpCache.get(key);

    /* filter list based on remaining tags */
    List<Playlist> result = filterPlaylistByTags(rpList.getItems(), tags);
    addExtraTags(result, tags, extraTags);
    return result;
  }

  private void addExtraTags(List<Playlist> result, List<String> knownTags,
      List<String> extraTags)
  {
    if (extraTags != null)
    {
      if (result != null)
      {
        for (Playlist p : result)
        {
          for (String tag : p.getTags().getTags())
          {
            if (((knownTags == null) || !knownTags.contains(tag)) &&
                !extraTags.contains(tag))
              extraTags.add(tag);
          }
        }
      }
    }
  }

  private List<Playlist> filterPlaylistByTags(List<Playlist> items,
      List<String> tags)
  {
    List<Playlist> filteredList = new ArrayList<Playlist>();
    if ((items != null) && (items.size() > 0))
    {
      for (Playlist p : items)
      {
        boolean add = true;
        for (String tag : tags)
        {
          if (!p.containsTag(tag))
          {
            add = false;
            break;
          }
        }

        if (add)
          filteredList.add(p);
      }
    }
    return filteredList;
  }

  private String mostSelectiveTag(List<String> tags) throws PlException
  {
    String tag = null;
    if ((tags != null) && (tags.size() > 0))
    {
      long minCount = Long.MAX_VALUE;

      for (String t : tags)
      {
        Long count = tcCache.get(t);
        if (count == null)
        {
          /* t doesn't even exist on any playlist */
          minCount = 0;
          tag = t;
        }
        else if (count < minCount)
        {
          minCount = count;
          tag = t;
        }
      }
      logger.debug("Returning with best tag: {}, minCount: {}", tag, String.valueOf(minCount));
    }
    return tag;
  }

  private void addIndexEntries(StorageTransaction txn, Playlist o)
      throws PlException
  {
    TagList tags = o.getTags();
    if (tags != null)
    {
      List<String> tagList = tags.getTags();
      if (tagList != null)
      {
        for (String tag : tagList)
        {
          TagIndex idx = new TagIndex(tag, o.getId());
          storageSvc.create(txn, idx);
        }
      }
    }
  }

  private void removeIndexEntries(StorageTransaction txn, Playlist o)
      throws PlException
  {
    TagList tags = o.getTags();
    if (tags != null)
    {
      List<String> tagList = tags.getTags();
      if (tagList != null)
      {
        for (String tag : tagList)
        {
          TagIndex idx = new TagIndex(tag, o.getId());
          storageSvc.delete(txn, idx);
        }
      }
    }
  }

  private void incrTagCounts(StorageTransaction txn, List<String> tags)
      throws PlException
  {
    if (tags != null)
    {
      for (String tag : tags)
      {
        TagCount tc = new TagCount();
        tc.setTag(tag);
        Long value = tcCache.get(tag);
        if (value == null)
        {
          tc.setCount(1);
          storageSvc.create(txn, tc);
        }
        else
        {
          tc.setCount(value.longValue() + 1);
          storageSvc.update(txn, tc);
        }
      }
    }
  }

  private void decrTagCounts(StorageTransaction txn, List<String> tags)
      throws PlException
  {
    if (tags != null)
    {
      for (String tag : tags)
      {
        TagCount tc = new TagCount();
        tc.setTag(tag);
        Long value = tcCache.get(tag);
        if (value != null)
        {
          tc.setCount(value.longValue() - 1);
          storageSvc.update(txn, tc);
        }
      }
    }
  }

  private void purgeCachedCounts(List<String> tags)
  {
    if (tags != null)
    {
      for (String tag : tags)
        tcCache.purge(tag);
    }
  }

  private void purgeCachedPlaylists(List<String> tags)
  {
    rpCache.purgeSelectEntries(new PurgeCallback(tags));
  }

  private static class PurgeCallback implements
      ICacheCallback<RankedPlaylistKey, RankedPlaylist>
  {
    private List<String> tagsToPurge;

    private PurgeCallback(List<String> tags)
    {
      tagsToPurge = tags;
    }

    @Override
    public boolean check(RankedPlaylistKey key, RankedPlaylist value)
    {
      logger.trace("Purging ranked playlist for: {}", SerDeUtils.serializeToJson(key));
      return tagsToPurge.contains(key.getTag());
    }
  }
}
