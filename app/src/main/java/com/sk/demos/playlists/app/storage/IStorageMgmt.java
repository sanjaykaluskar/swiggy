/**
 * IStorageMgmt.java
 */
package com.sk.demos.playlists.app.storage;

import com.sk.demos.playlists.app.exceptions.PlException;

/**
 * @author sanjay
 *
 */
@SuppressWarnings("rawtypes")
public interface IStorageMgmt
{
  /* admin */
  public void install() throws PlException;
  
  public void uninstall() throws PlException;

  /* transaction management */
  public StorageTransaction beginTxn() throws PlException;

  public void commitTxn(StorageTransaction tx) throws PlException;

  public void abortTxn(StorageTransaction tx) throws PlException;

  /* object management */
  public void create(StorageTransaction txn, Object key) throws PlException;

  public Object read(StorageTransaction txn, Object key, Class<?> valueType)
      throws PlException;

  public void update(StorageTransaction txn, Object o) throws PlException;

  public void delete(StorageTransaction txn, Object o) throws PlException;

  public StorageResultSet search(StorageTransaction txn, String condition,
      Class<?> valueType)
          throws PlException;
}
