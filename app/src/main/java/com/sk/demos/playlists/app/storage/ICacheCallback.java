/**
 * ICacheCallback.java
 */
package com.sk.demos.playlists.app.storage;

/**
 * @author sanjay
 *
 */
public interface ICacheCallback<KeyType, ValueType>
{
  public boolean check(KeyType key, ValueType value);
}
