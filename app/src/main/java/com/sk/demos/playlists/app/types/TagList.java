/**
 * TagList.java
 */
package com.sk.demos.playlists.app.types;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import com.sk.demos.playlists.app.utils.StringUtils;

import lombok.Getter;
import lombok.Setter;

/**
 * @author sanjay
 *
 *         List of tags is defined as a separate class primarily to support
 *         serialization / deserialization.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "tagList")
@Getter
@Setter
public class TagList
{
  /** list of tags applied to the playlist */
  private List<String> tags;

  /**
   * Checks if the list contains a given tag.
   * 
   * @input name - tag to check
   * @return true if list contains the given tag else false. If input tag is
   *         null or empty, returns false.
   */
  public boolean containsTag(String name)
  {
    boolean ret = false;
    if ((tags != null) && !StringUtils.isNullOrEmpty(name))
    {
      for (String tag : tags)
      {
        if (tag.equals(name))
        {
          ret = true;
          break;
        }
      }
    }

    return ret;
  }

  /**
   * Adds given tag to the list, if it doesn't already exist. The operation is a
   * no-op if the tag already exists.
   * 
   * @input name - tag to be added
   * @return true if the tag was newly added else false (if it already existed).
   */
  public boolean addTag(String name)
  {
    boolean added = false;
    if (!containsTag(name))
    {
      if (tags == null)
        tags = new ArrayList<String>();

      tags.add(name);
      added = true;
    }

    return added;
  }
}
