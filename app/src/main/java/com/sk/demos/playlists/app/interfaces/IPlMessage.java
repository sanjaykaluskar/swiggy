/**
 * IPlMessage.java
 */
package com.sk.demos.playlists.app.interfaces;

/**
 * @author sanjay
 *
 */
public interface IPlMessage
{
  public String getMessage();
}
