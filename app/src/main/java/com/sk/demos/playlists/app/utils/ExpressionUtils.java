package com.sk.demos.playlists.app.utils;

/**
 * ExpressionUtils.java
 */

/**
 * @author sanjay
 *
 */
public class ExpressionUtils
{
  /**
   * a safe way to check for equality of objects
   * 
   * @param a
   *          - object to check for equality
   * @param b
   *          - object to check for equality
   * 
   * @return true if both objects are null or equal, else false
   */
  public static boolean safeEqual(Object a, Object b)
  {
    return (a == null) ? (b == null) : a.equals(b);
  }

  /**
   * a safe way to convert an enum to string
   * 
   * @param e
   *          - enum
   * 
   * @return null if input is null else string value of the enum
   */
  public static String safeString(Enum<?> e)
  {
    return (e == null) ? null : e.toString();
  }
}
