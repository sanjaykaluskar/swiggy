package com.sk.demos.playlists.app.types;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Playlist
{
  /**
   * compact id but with sufficient range to provide a unique id for each item
   */
  private long id;

  /** descriptive name, not necessarily unique */
  private String name;

  /** number of likes */
  private long likes;

  /** number of plays */
  private long plays;

  /** list of tags applied to the playlist */
  private TagList tags;

  /**
   * Checks if the playlist contains a given tag.
   * 
   * @input name - tag to check
   * @return true if playlist contains the given tag else false. If input tag is
   *         null or empty, returns false.
   */
  public boolean containsTag(String name)
  {
    return (tags != null) && tags.containsTag(name);
  }

  /**
   * Adds given tag to the playlist, if it doesn't already exist. The operation
   * is a no-op if the tag already exists.
   * 
   * @input name - tag to be added
   * @return true if the tag was newly added else false (if it already existed).
   */
  public boolean addTag(String name)
  {
    if (tags == null)
      tags = new TagList();
    return tags.addTag(name);
  }
}
