/**
 * StorageMgmtService.java
 */
package com.sk.demos.playlists.app.storage;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.ServiceLoader;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sk.demos.playlists.app.exceptions.PLMessages;
import com.sk.demos.playlists.app.exceptions.PlException;
import com.sk.demos.playlists.app.utils.SerDeUtils;

/**
 * @author sanjay
 *
 */
@SuppressWarnings("rawtypes")
public class StorageMgmtService implements IStorageMgmt
{
  private static Map<Class<?>, IRecordHandler> handlers;

  private static ServiceLoader<IRecordHandler> recordHandlerLoader =
      ServiceLoader
          .load(IRecordHandler.class);

  static
  {
    /* load handlers */
    handlers = new HashMap<Class<?>, IRecordHandler>();
    for (IRecordHandler h : recordHandlerLoader)
    {
      handlers.put(h.getSupportedType(), h);
    }
  }

  private static StorageMgmtService INSTANCE = new StorageMgmtService();

  private static final Logger logger = LoggerFactory.getLogger(
      StorageMgmtService.class);

  public static IStorageMgmt getService()
  {
    return INSTANCE;
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.sk.demos.playlists.app.storage.IStorageMgmt#beginTxn()
   */
  @Override
  public StorageTransaction beginTxn() throws PlException
  {
    ConnectionManager cxnMgr = new ConnectionManager();
    StorageTransaction txn = new StorageTransaction(cxnMgr.getConnection());
    return txn;
  }

  /*
   * (non-Javadoc)
   * 
   * @see
   * com.sk.demos.playlists.app.storage.IStorageMgmt#commitTxn(com.sk.demos.
   * playlists.app.storage.StorageTransaction)
   */
  @Override
  public void commitTxn(StorageTransaction tx) throws PlException
  {
    Connection cxn = tx.getConnection();
    try
    {
      if ((cxn != null) && !cxn.isClosed())
        cxn.commit();
    }
    catch (Exception e)
    {
      try
      {
        if ((cxn != null) && !cxn.isClosed())
          cxn.close();
      }
      catch (SQLException e1)
      {
        throw new PlException(PLMessages.PL_DB_OPERATION, e1);
      }
      throw new PlException(PLMessages.PL_DB_OPERATION, e);
    }
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.sk.demos.playlists.app.storage.IStorageMgmt#abortTxn(com.sk.demos.
   * playlists.app.storage.StorageTransaction)
   */
  @Override
  public void abortTxn(StorageTransaction tx) throws PlException
  {
    Connection cxn = tx.getConnection();
    try
    {
      if ((cxn != null) && (!cxn.isClosed()))
        cxn.rollback();
    }
    catch (Exception e)
    {
      try
      {
        if ((cxn != null) && !cxn.isClosed())
          cxn.close();
      }
      catch (SQLException e1)
      {
        throw new PlException(PLMessages.PL_DB_OPERATION, e1);
      }
      throw new PlException(PLMessages.PL_DB_OPERATION, e);
    }
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.sk.demos.playlists.app.storage.IStorageMgmt#create(com.sk.demos.
   * playlists.app.storage.StorageTransaction, java.lang.Object)
   */
  @SuppressWarnings("unchecked")
  @Override
  public void create(StorageTransaction txn, Object o) throws PlException
  {
    Connection cxn = txn.getConnection();
    IRecordHandler h = handlers.get(o.getClass());
    if (h == null)
      throw new PlException(PLMessages.PL_INTERNAL_ERROR);
    h.create(cxn, o);
    logger.trace("Created object - {}", SerDeUtils.serializeToJson(o));
  }

  /*
   * (non-Javadoc)
   * 
   * @see
   * com.sk.demos.playlists.app.storage.IStorageMgmt#read(com.sk.demos.playlists
   * .app.storage.StorageTransaction, java.lang.Object)
   */
  @SuppressWarnings("unchecked")
  @Override
  public Object read(StorageTransaction txn, Object key, Class<?> valueType)
      throws PlException
  {
    Connection cxn = txn.getConnection();
    Object o = null;
    IRecordHandler h = handlers.get(valueType);
    if (h == null)
      throw new PlException(PLMessages.PL_INTERNAL_ERROR);
    o = h.read(cxn, key);

    logger.trace("Read object - key: {}, value: {}",
        SerDeUtils.serializeToJson(key), SerDeUtils.serializeToJson(o));
    return o;
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.sk.demos.playlists.app.storage.IStorageMgmt#update(com.sk.demos.
   * playlists.app.storage.StorageTransaction, java.lang.Object)
   */
  @SuppressWarnings("unchecked")
  @Override
  public void update(StorageTransaction txn, Object o) throws PlException
  {
    Connection cxn = txn.getConnection();
    IRecordHandler h = handlers.get(o.getClass());
    if (h == null)
      throw new PlException(PLMessages.PL_INTERNAL_ERROR);
    h.update(cxn, o);
    logger.trace("Updated object - {}", SerDeUtils.serializeToJson(o));
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.sk.demos.playlists.app.storage.IStorageMgmt#delete(com.sk.demos.
   * playlists.app.storage.StorageTransaction, java.lang.Object)
   */
  @SuppressWarnings("unchecked")
  @Override
  public void delete(StorageTransaction txn, Object o) throws PlException
  {
    Connection cxn = txn.getConnection();
    IRecordHandler h = handlers.get(o.getClass());
    if (h == null)
      throw new PlException(PLMessages.PL_INTERNAL_ERROR);
    h.delete(cxn, o);
    logger.trace("Deleted object - {}", SerDeUtils.serializeToJson(o));
  }

  /*
   * (non-Javadoc)
   * 
   * @see
   * com.sk.demos.playlists.app.storage.IStorageMgmt#search(java.sql.Connection,
   * java.lang.Class, java.lang.String)
   */
  @Override
  public StorageResultSet search(StorageTransaction txn, String condition,
      Class<?> valueType)
          throws PlException
  {
    Connection cxn = txn.getConnection();
    StorageResultSet rs = null;
    IRecordHandler h = handlers.get(valueType);
    if (h == null)
      throw new PlException(PLMessages.PL_INTERNAL_ERROR);
    rs = h.search(cxn, condition);

    return rs;
  }

  @Override
  public void install() throws PlException
  {
    StorageTransaction txn = beginTxn();
    try
    {
      for (IRecordHandler h : handlers.values())
      {
        h.createContainer(txn.getConnection());
      }
      commitTxn(txn);
    }
    catch (Exception e)
    {
      abortTxn(txn);
      throw new PlException(PLMessages.PL_DB_OPERATION, e);
    }
  }

  @Override
  public void uninstall() throws PlException
  {
    StorageTransaction txn = beginTxn();
    try
    {
      for (IRecordHandler h : handlers.values())
      {
        h.deleteContainer(txn.getConnection());
      }
      commitTxn(txn);
    }
    catch (Exception e)
    {
      abortTxn(txn);
      throw new PlException(PLMessages.PL_DB_OPERATION, e);
    }
  }
}
