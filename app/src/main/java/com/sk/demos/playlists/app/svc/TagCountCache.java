/**
 * TagCountCache.java
 */
package com.sk.demos.playlists.app.svc;

import com.sk.demos.playlists.app.exceptions.PlException;
import com.sk.demos.playlists.app.storage.IStorageMgmt;
import com.sk.demos.playlists.app.storage.SimpleCache;
import com.sk.demos.playlists.app.storage.StorageMgmtService;
import com.sk.demos.playlists.app.storage.StorageTransaction;
import com.sk.demos.playlists.app.types.TagCount;

/**
 * @author sanjay
 *
 */
public class TagCountCache extends SimpleCache<String, Long>
{
  private static final int TARGET_SIZE = 100;

  private static final TagCountCache INSTANCE = new TagCountCache();

  private IStorageMgmt storageSvc;

  protected TagCountCache()
  {
    super(TARGET_SIZE);
    storageSvc = StorageMgmtService.getService();
  }

  @Override
  protected Long load(String key) throws PlException
  {
    StorageTransaction txn = storageSvc.beginTxn();
    Long tagCount = null;
    TagCount o;
    try
    {
      o = (TagCount) storageSvc.read(txn, key, TagCount.class);
      if (o != null)
        tagCount = o.getCount();
      storageSvc.commitTxn(txn);
    }
    catch (Exception e)
    {
      storageSvc.abortTxn(txn);
      throw(e);
    }
    return tagCount;
  }

  static public TagCountCache getCache()
  {
    return INSTANCE;
  }
}
