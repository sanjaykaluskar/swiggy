/**
 * SimpleCache.java
 */
package com.sk.demos.playlists.app.storage;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map.Entry;

import com.sk.demos.playlists.app.exceptions.PlException;
import com.sk.demos.playlists.app.utils.SerDeUtils;

/**
 * @author sanjay
 *
 *         A simple non-write through cache.
 */
abstract public class SimpleCache<KeyType, ValueType>
{
  private static final float LOAD_FACTOR = 0.750f;

  private static final Logger logger = LoggerFactory.getLogger(
      SimpleCache.class);

  private LinkedHashMap<KeyType, ValueType> map;

  protected SimpleCache(final int targetSize)
  {
    int hashTableCapacity = (int) Math.ceil(targetSize / LOAD_FACTOR) + 1;
    map = new LinkedHashMap<KeyType, ValueType>(
        hashTableCapacity,
        LOAD_FACTOR, true) {
      // (an anonymous inner class)
      private static final long serialVersionUID = 1;

      @Override
      protected synchronized boolean removeEldestEntry(
          Map.Entry<KeyType, ValueType> eldest)
      {
        logger.trace("LRU callback - size: {}", String.valueOf(size()));
        return (size() > targetSize);
      }
    };
  }

  private synchronized void addValue(KeyType key, ValueType value)
  {
    map.put(key, value);
    logger.trace("Added - key: {}, value: {}", SerDeUtils.serializeToJson(key),
        SerDeUtils.serializeToJson(value));
  }

  public ValueType get(KeyType key) throws PlException
  {
    ValueType value = map.get(key);
    if (value == null)
    {
      value = load(key);
      addValue(key, value);
    }
    logger.trace("Get - key: {}, value: {}", SerDeUtils.serializeToJson(key),
        SerDeUtils.serializeToJson(value));
    return value;
  }

  public synchronized void purge(KeyType key)
  {
    map.remove(key);
    logger.trace("Purged - key: {}", SerDeUtils.serializeToJson(key));
  }

  public synchronized void purgeSelectEntries(
      ICacheCallback<KeyType, ValueType> cbk)
  {
    Set<Entry<KeyType, ValueType>> entries = map.entrySet();
    for (Entry<KeyType, ValueType> e : entries)
    {
      if (cbk.check(e.getKey(), e.getValue()))
        entries.remove(e);
    }
  }

  protected abstract ValueType load(KeyType key) throws PlException;
}
