/**
 * TagIndexHandler.java
 */
package com.sk.demos.playlists.app.storage.handlers;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.sk.demos.playlists.app.exceptions.PLMessages;
import com.sk.demos.playlists.app.exceptions.PlException;
import com.sk.demos.playlists.app.storage.RecordHandler;
import com.sk.demos.playlists.app.storage.StorageResultSet;
import com.sk.demos.playlists.app.types.TagIndex;

/**
 * @author sanjay
 * 
 */
public class TagIndexHandler extends RecordHandler<TagIndex, TagIndex>
{
  public class TagIndexResultSet extends StorageResultSet<TagIndex>
  {
    public TagIndexResultSet(ResultSet rs, Statement stmt, Connection cxn)
    {
      super(rs, stmt, cxn);
    }

    @Override
    public TagIndex next() throws PlException
    {
      try
      {
        TagIndex o = null;

        if (rs.next())
        {
          o = new TagIndex(rs.getString("tag"), rs.getLong("playlist_id"));
        }

        return o;
      }
      catch (Exception e)
      {
        try
        {
          if ((cxn != null) && !cxn.isClosed())
            cxn.close();
        }
        catch (SQLException e1)
        {
          throw new PlException(PLMessages.PL_DB_OPERATION, e1);
        }
        throw new PlException(PLMessages.PL_DB_OPERATION, e);
      }
    }
  }

  private static final String STMT_CREATE_TABLE = "create table tagindices ("
      + "tag varchar2(4000), "
      + "playlist_id number)";

  private static final String STMT_CREATE_INDEX =
      "create index i_tagindices on tagindices (tag, playlist_id)";

  private static final String STMT_DROP = "drop table tagindices";

  private static final String STMT_SELECT = "select "
      + "tag, playlist_id "
      + "from tagindices "
      + "where tag = ? and playlist_id = ?";

  private static final String STMT_INSERT = "insert into tagindices"
      + "(tag, playlist_id) "
      + "values(?, ?)";

  private static final String STMT_DELETE = "delete from tagindices "
      + "where (tag = ? and playlist_id = ?)";

  private static final String STMT_SEARCH = "select "
      + "tag, playlist_id "
      + "from tagindices "
      + "where (%s)";

  public TagIndexHandler()
  {
    super(TagIndex.class);
  }

  @Override
  public void createContainer(Connection c)
      throws PlException
  {
    try
    {
      Statement stmt = c.createStatement();
      stmt.execute(STMT_CREATE_TABLE);
      stmt.execute(STMT_CREATE_INDEX);
      stmt.close();
    }
    catch (Exception e)
    {
      throw new PlException(PLMessages.PL_DB_OPERATION, e);
    }
  }

  @Override
  public void deleteContainer(Connection c)
      throws PlException
  {
    try
    {
      Statement stmt = c.createStatement();
      stmt.execute(STMT_DROP);
      stmt.close();
    }
    catch (Exception e)
    {
      throw new PlException(PLMessages.PL_DB_OPERATION, e);
    }
  }

  @Override
  public void create(Connection c, TagIndex o)
      throws PlException
  {
    try
    {
      PreparedStatement stmt = c.prepareStatement(STMT_INSERT);
      stmt.setString(1, o.getName());
      stmt.setLong(2, o.getId());
      stmt.executeUpdate();
      stmt.close();
    }
    catch (Exception e)
    {
      throw new PlException(PLMessages.PL_DB_OPERATION, e);
    }
  }

  @Override
  public TagIndex read(Connection c, TagIndex key) throws PlException
  {
    TagIndex o = null;

    try
    {
      PreparedStatement stmt = c.prepareStatement(STMT_SELECT);
      stmt.setString(1, key.getName());
      stmt.setLong(2, key.getId());
      ResultSet rs = stmt.executeQuery();
      if (rs.next())
      {
        o = new TagIndex(rs.getString("tag"), rs.getLong("playlist_id"));
        stmt.close();
      }
    }
    catch (Exception e)
    {
      o = null;
    }
    return o;
  }

  @Override
  public void delete(Connection c, TagIndex o) throws PlException
  {
    try
    {
      PreparedStatement stmt = c.prepareStatement(STMT_DELETE);
      stmt.setString(1, o.getName());
      stmt.setLong(2, o.getId());
      stmt.executeUpdate();
      stmt.close();
    }
    catch (Exception e)
    {
      throw new PlException(PLMessages.PL_DB_OPERATION, e);
    }
  }

  @Override
  public TagIndexResultSet search(Connection c, String condition)
      throws PlException
  {
    TagIndexResultSet rs = null;

    try
    {
      String sql = String.format(STMT_SEARCH, condition);
      PreparedStatement stmt = c.prepareStatement(sql);
      ResultSet resultSet = stmt.executeQuery();
      if (resultSet != null)
      {
        rs = new TagIndexResultSet(resultSet, stmt, c);
      }
      return rs;
    }
    catch (Exception e)
    {
      throw new PlException(PLMessages.PL_DB_OPERATION, e);
    }
  }
}
