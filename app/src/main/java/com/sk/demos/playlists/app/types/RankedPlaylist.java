/**
 * RankedPlaylist.java
 */
package com.sk.demos.playlists.app.types;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

/**
 * @author sanjay
 *
 */
@Getter
@Setter
public class RankedPlaylist
{
  private List<Playlist> items;

  /**
   * Adds a playlist to the list
   * 
   * @param p
   *          - playlist to add
   */
  public void addPlaylist(Playlist p)
  {
    if (items == null)
      items = new ArrayList<Playlist>();
    items.add(p);
  }
}
