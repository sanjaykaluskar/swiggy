/**
 * IOUtils.java
 */
package com.sk.demos.playlists.app.utils;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import com.sk.demos.playlists.app.exceptions.PLMessages;
import com.sk.demos.playlists.app.exceptions.PlException;
import com.sk.demos.playlists.app.types.Playlist;

/**
 * @author skaluska
 * 
 */
public class IOUtils
{
  private static final int NUM_PLAYLIST_FIELDS = 4;

  /**
   * Reads playlists from the specified csv formatted file (with UTF-8
   * encoding). Each playlist is assumed to be on a separate line. Fields are
   * separated by commas. Fields are assumed to be in the following order:
   * 
   * name, plays, likes, tags separated by spaces
   * 
   * @param filePath
   *          - location of the file
   * @return list of read playlists
   * @throws PlException
   *           in case of errors such as missing file or bad format
   *
   */
  public static List<Playlist> readPlaylistsFromCsv(String filePath)
      throws PlException
  {
    List<Playlist> playlists = new ArrayList<Playlist>();

    if (StringUtils.isNullOrEmpty(filePath))
      throw new PlException(PLMessages.PL_MISSING_FILE_NAME);

    BufferedReader reader = null;
    try
    {
      String line;
      int lineNo = 0;

      reader = new BufferedReader(new InputStreamReader(
          new FileInputStream(filePath), StandardCharsets.UTF_8));
      while ((line = reader.readLine()) != null)
      {
        lineNo++;
        if (line.length() > 0)
        {
          String[] fields = line.split(StringUtils.STR_COMMA);
          if ((fields != null) && (fields.length > 0))
          {
            if (fields.length != NUM_PLAYLIST_FIELDS)
              throw new PlException(PLMessages.PL_WRONG_NUM_FIELDS, filePath,
                  String.valueOf(lineNo), String.valueOf(fields.length),
                  String.valueOf(NUM_PLAYLIST_FIELDS));

            Playlist pl = new Playlist();
            pl.setName(fields[0].trim());
            pl.setPlays(Long.parseLong(fields[1].trim()));
            pl.setLikes(Long.parseLong(fields[2].trim()));
            String tags = fields[3].trim();
            if (!StringUtils.isNullOrEmpty(tags))
            {
              String[] tagList = tags.split(StringUtils.STR_SPACE);
              if ((tagList != null) && (tagList.length > 0))
              {
                for (String tag : tagList)
                  pl.addTag(tag);
              }
            }
            playlists.add(pl);
          }
        }
      }
    }
    catch (FileNotFoundException e)
    {
      throw new PlException(PLMessages.PL_FILE_NOT_FOUND, e, filePath);
    }
    catch (IOException e)
    {
      throw new PlException(PLMessages.PL_INTERNAL_ERROR, e);
    }
    finally
    {
      if (reader != null)
      {
        try
        {
          reader.close();
        }
        catch (IOException e)
        {
          throw new PlException(PLMessages.PL_INTERNAL_ERROR, e);
        }
      }
    }

    return playlists;
  }
}
