/**
 * IRecordHandler.java
 */
package com.sk.demos.playlists.app.storage;

import java.sql.Connection;

import com.sk.demos.playlists.app.exceptions.PlException;

/**
 * @author sanjay
 * 
 *         This is a simplified interface to read and write a persisted record
 *         atomically.
 *
 *         It defines methods to store objects of type ObjectType, which contain
 *         a key field of type KeyType.
 */
public interface IRecordHandler<KeyType, ObjectType>
{
  public Class<?> getSupportedType();

  /**
   * createContainer
   * 
   * @param cxn
   *          - connection to use
   */
  public void createContainer(Connection cxn) throws PlException;

  /**
   * deleteContainer
   */
  public void deleteContainer(Connection cxn) throws PlException;

  /**
   * create specified object within the storage with contents from the input
   * object.
   * 
   * @param cxn
   *          - connection to use
   * @param o
   *          - input object with content
   */
  public void create(Connection cxn, ObjectType o) throws PlException;

  /**
   * read object of specified class with the given key
   * 
   * @param cxn
   *          - connection to use
   * @param key
   *          - key for the object to read
   * 
   * @return object with the specified key if it exists else null
   */
  public ObjectType read(Connection cxn, KeyType key) throws PlException;

  /**
   * update specified object based on contents of the updated object
   * 
   * @param cxn
   *          - connection to use
   * @param o
   *          - input object with updated contents
   */
  public void update(Connection cxn, ObjectType o) throws PlException;

  /**
   * deletes object with key specified in the input object
   * 
   * @param cxn
   *          - connection to use
   * @param o
   *          - input object to delete
   */
  public void delete(Connection cxn, ObjectType o) throws PlException;

  /**
   * search for objects satisfying a condition
   * 
   * @param cxn
   *          - connection to use
   * @param condition
   *          - search condition
   */
  public StorageResultSet<ObjectType> search(Connection cxn, String condition)
      throws PlException;
}
