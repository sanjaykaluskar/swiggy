/**
 * RankedPlaylistKey.java
 */
package com.sk.demos.playlists.app.types;

import lombok.Data;

/**
 * @author sanjay
 *
 */
@Data
public class RankedPlaylistKey
{
  private String tag;
  
  private long startPosition;
  
  private long endPosition;
}
