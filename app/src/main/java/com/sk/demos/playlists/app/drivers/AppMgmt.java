/**
 * AppMgmt.java
 */
package com.sk.demos.playlists.app.drivers;

import com.sk.demos.playlists.app.exceptions.PlException;
import com.sk.demos.playlists.app.storage.StorageMgmtService;

/**
 * @author sanjay
 *
 */
public class AppMgmt
{
  public static void install() throws PlException
  {
    StorageMgmtService.getService().install();
  }
  
  public static void uninstall() throws PlException
  {
    StorageMgmtService.getService().uninstall();
  }
  
  public static void startup() throws PlException
  {
  }
  
  public static void shutdown() throws PlException
  {
  }
}
