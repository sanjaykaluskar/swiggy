/**
 * StorageTransaction.java
 */
package com.sk.demos.playlists.app.storage;

import java.sql.Connection;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * @author sanjay
 *
 */
@AllArgsConstructor
@Getter
@Setter
public class StorageTransaction
{
  private Connection connection;
}
