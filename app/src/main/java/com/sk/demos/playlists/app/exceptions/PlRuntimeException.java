/**
 * PlRuntimeException.java
 */
package com.sk.demos.playlists.app.exceptions;

import com.sk.demos.playlists.app.interfaces.IPlMessage;

/**
 * @author skaluska
 *
 */
public class PlRuntimeException extends RuntimeException
{
  /** PlRuntimeException.java */
  private static final long serialVersionUID = 1L;
  
  private IPlMessage message;

  public IPlMessage getMessageEnum()
  {
    return message;
  }

  public PlRuntimeException(PLMessages m)
  {
    super(m.getMessage());
    message = m;
  }

  public PlRuntimeException(PLMessages m, String... arg)
  {
    super(m.getMessage());
    message = m;
  }

  public PlRuntimeException(PLMessages m, Exception e)
  {
    super(m.getMessage());
    message = m;
  }

  public PlRuntimeException(PLMessages m, Exception e, String... strings)
  {
    super(m.getMessage());
    message = m;
  }
}
