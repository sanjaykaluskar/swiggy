/**
 * TagCountHandler.java
 */
package com.sk.demos.playlists.app.storage.handlers;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

import com.sk.demos.playlists.app.exceptions.PLMessages;
import com.sk.demos.playlists.app.exceptions.PlException;
import com.sk.demos.playlists.app.storage.RecordHandler;
import com.sk.demos.playlists.app.types.TagCount;

/**
 * @author sanjay
 * 
 */
public class TagCountHandler extends RecordHandler<String, TagCount>
{
  private static final String STMT_CREATE_TABLE = "create table tagcounts ("
      + "tag varchar2(4000) primary key, "
      + "count number)";

  private static final String STMT_DROP = "drop table tagcounts";

  private static final String STMT_SELECT = "select "
      + "tag, count "
      + "from tagcounts "
      + "where tag = ?";

  private static final String STMT_INSERT = "insert into tagcounts"
      + "(tag, count) "
      + "values(?, ?)";

  private static final String STMT_UPDATE = "update tagcounts set "
      + "    count = ? "
      + "where (tag = ?)";

  private static final String STMT_DELETE = "delete from tagcounts "
      + "where (tag = ?)";

  public TagCountHandler()
  {
    super(TagCount.class);
  }

  @Override
  public void createContainer(Connection c)
      throws PlException
  {
    try
    {
      Statement stmt = c.createStatement();
      stmt.execute(STMT_CREATE_TABLE);
      stmt.close();
    }
    catch (Exception e)
    {
      throw new PlException(PLMessages.PL_DB_OPERATION, e);
    }
  }

  @Override
  public void deleteContainer(Connection c)
      throws PlException
  {
    try
    {
      Statement stmt = c.createStatement();
      stmt.execute(STMT_DROP);
      stmt.close();
    }
    catch (Exception e)
    {
      throw new PlException(PLMessages.PL_DB_OPERATION, e);
    }
  }

  @Override
  public void create(Connection c, TagCount o)
      throws PlException
  {
    try
    {
      PreparedStatement stmt = c.prepareStatement(STMT_INSERT);
      stmt.setString(1, o.getTag());
      stmt.setLong(2, o.getCount());
      stmt.executeUpdate();
      stmt.close();
    }
    catch (Exception e)
    {
      throw new PlException(PLMessages.PL_DB_OPERATION, e);
    }
  }

  @Override
  public TagCount read(Connection c, String key) throws PlException
  {
    TagCount o = null;

    try
    {
      PreparedStatement stmt = c.prepareStatement(STMT_SELECT);
      stmt.setString(1, key);
      ResultSet rs = stmt.executeQuery();
      if (rs.next())
      {
        o = new TagCount();
        o.setTag(rs.getString("tag"));
        o.setCount(rs.getLong("count"));
        stmt.close();
      }
    }
    catch (Exception e)
    {
      o = null;
    }
    return o;
  }

  @Override
  public void update(Connection c, TagCount o) throws PlException
  {
    try
    {
      PreparedStatement stmt = c.prepareStatement(STMT_UPDATE);
      stmt.setLong(1, o.getCount());
      stmt.setString(2, o.getTag());
      stmt.executeUpdate();
      stmt.close();
    }
    catch (Exception e)
    {
      throw new PlException(PLMessages.PL_DB_OPERATION, e);
    }
  }

  @Override
  public void delete(Connection c, TagCount o) throws PlException
  {
    try
    {
      PreparedStatement stmt = c.prepareStatement(STMT_DELETE);
      stmt.setString(1, o.getTag());
      stmt.executeUpdate();
      stmt.close();
    }
    catch (Exception e)
    {
      throw new PlException(PLMessages.PL_DB_OPERATION, e);
    }
  }
}
