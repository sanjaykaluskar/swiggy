/**
 * SerDeUtils.java
 */
package com.sk.demos.playlists.app.utils;

import java.io.ByteArrayOutputStream;
import java.io.StringReader;
import java.nio.charset.StandardCharsets;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import org.codehaus.jackson.map.ObjectMapper;

import com.sk.demos.playlists.app.exceptions.PLMessages;
import com.sk.demos.playlists.app.exceptions.PlRuntimeException;
import com.sk.demos.playlists.app.types.Playlist;

/**
 * @author skaluska
 * 
 */
public class SerDeUtils
{
  /**
   * Serializes specified object into XML using JAXB marshaller.
   * 
   * @param inputObject - input object to serialize
   * @param classes - list of classes present in the input object
   * 
   * @return string containing the serialized XML in UTF-8 encoding
   */
  public static String serializeUsingJaxb(Object inputObject, Class<?>...classes)
  {
    String outputString = null;

    if (inputObject != null)
    {
      try
      {
        ByteArrayOutputStream bout = new ByteArrayOutputStream();
        JAXBContext jaxbCtx = JAXBContext.newInstance(classes);
        Marshaller m = jaxbCtx.createMarshaller();
        m.marshal(inputObject, bout);
        byte[] outputBytes = bout.toByteArray();
        outputString = new String(outputBytes, StandardCharsets.UTF_8);
      }
      catch (JAXBException e)
      {
        throw new PlRuntimeException(PLMessages.PL_INTERNAL_ERROR, e);
      }
    }

    return outputString;
  }

  /**
   * Deserializes an object using JAXB unmarshaller from an input XML string.
   * 
   * @param inputString - input string containing the object XML in UTF-8 encoding
   * @return deserialized object
   */
  public static Object deSerializeUsingJaxb(String inputString)
  {
    Object output = null;

    if (!StringUtils.isNullOrEmpty(inputString))
    {
      try
      {
        StringReader xmlReader = new StringReader(new String(inputString));
        JAXBContext jaxbCtx = JAXBContext.newInstance(Playlist.class);
        Unmarshaller um = jaxbCtx.createUnmarshaller();
        output = um.unmarshal(xmlReader);
      }
      catch (JAXBException e)
      {
        throw new PlRuntimeException(PLMessages.PL_INTERNAL_ERROR, e);
      }
    }

    return output;
  }
  
  public static String serializeToJson(Object o)
  {
    ObjectMapper om = new ObjectMapper();
    String s = "";
    try
    {
      s = om.writeValueAsString(o);
    }
    catch (Exception e)
    {
      throw new PlRuntimeException(PLMessages.PL_INTERNAL_ERROR, e);
    }

    return s;
  }
}
