/**
 * TagIndex.java
 */
package com.sk.demos.playlists.app.types;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * @author sanjay
 *
 */
@Getter
@Setter
@AllArgsConstructor
public class TagIndex
{
  private String name;

  private long id;
}
