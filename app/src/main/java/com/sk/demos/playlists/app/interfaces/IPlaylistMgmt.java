/**
 * IPlaylistMgmt.java
 */
package com.sk.demos.playlists.app.interfaces;

import java.util.List;

import com.sk.demos.playlists.app.exceptions.PlException;
import com.sk.demos.playlists.app.types.Playlist;

/**
 * @author sanjay
 *
 */
public interface IPlaylistMgmt
{
  /**
   * Creates a new playlist and populates it with fields from the input (except
   * the id).
   * 
   * @param o
   *          - input playlist containing values for fields
   * @return id of the newly created playlist
   * @throws PlException
   *           when input playlist is null or has problems
   */
  public long create(Playlist o) throws PlException;

  /**
   * Retrieves a playlist using its id.
   * 
   * @param id
   *          - id of playlist to retrieve
   * @return playlist
   * @throws PlException
   *           if the input id is invalid
   */
  public Playlist read(long id) throws PlException;

  /**
   * Updates playlist using field values in the input object.
   * 
   * @param o
   *          - updated object
   * @throws PlException
   *           if the input object doesn't correspond to an existing playlist
   */
  public void update(Playlist o) throws PlException;

  /**
   * Deletes specified playlist.
   * 
   * @input o - playlist to delete
   * @throws PlException
   *           if the input object doesn't correspond to an existing playlist
   */
  public void delete(Playlist o) throws PlException;

  /**
   * Finds playlists that have all the specified input tags, orders them by
   * descending number of plays and likes, and returns them based on that order.
   * 
   * In general this can be a very long list and to support scrolling up or down
   * (in response to user actions) the caller can specify a start position and
   * maximum number of results to return.
   * 
   * Extra tags that exist in the returned list are optionally returned (this is
   * helpful for a user to narrow down the list while browsing).
   * 
   * @param tags
   *          - input tags to search; each returned playlist contains all these
   *          tags
   * @param extraTags
   *          - additional tags that exist in the returned playlists are added
   *          to this list if it is not null
   * @param start
   *          - start position (in the ordered list) from which playlists are
   *          returned
   * @param limit
   *          - maximum number of playlists to be returned
   */
  public List<Playlist> lookupByTags(List<String> tags, List<String> extraTags,
      int start, int limit) throws PlException;
}
