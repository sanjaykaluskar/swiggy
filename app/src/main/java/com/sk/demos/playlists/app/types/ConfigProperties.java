package com.sk.demos.playlists.app.types;

public enum ConfigProperties
{
  DB_TYPE("DB_TYPE"),
  DB_HOST("DB_HOST"),
  DB_PORT("DB_PORT"),
  DB_SID("DB_SID"),
  DB_USER("DB_USER"),
  DB_PASSWORD("DB_PASSWORD");

  private final String value;

  ConfigProperties(String v)
  {
    value = v;
  }

  public String value()
  {
    return value;
  }

  public static ConfigProperties fromValue(String v)
  {
    if (v == null)
      return null;

    for (ConfigProperties c : ConfigProperties.values())
    {
      if (c.value.equals(v))
      {
        return c;
      }
    }
    throw new IllegalArgumentException(v);
  }
}
