/**
 * StringUtils.java
 */
package com.sk.demos.playlists.app.utils;

import java.util.List;

/**
 * @author sanjay
 *
 */
public class StringUtils
{
  public static final String STR_COMMA = ",";

  public static final String STR_SPACE = " ";

  /**
   * Checks if the input is either null or an empty string (0 length).
   * 
   * @input s - input string to check
   * @return true / false
   */
  public static boolean isNullOrEmpty(String s)
  {
    return (s == null) || (s.length() == 0);
  }

  public static String concatenate(List<String> list, String delim)
  {
    String ret = null;

    if (list != null)
    {
      StringBuilder sb = new StringBuilder();
      boolean first = true;
      for (String s : list)
      {
        if (!first && (delim != null))
          sb.append(delim);

        sb.append(s);
        first = false;
      }
      
      ret = sb.toString();
    }
    return ret;
  }
}
