/**
 * RecordHandler.java
 */
package com.sk.demos.playlists.app.storage;

import java.sql.Connection;

import com.sk.demos.playlists.app.exceptions.PLMessages;
import com.sk.demos.playlists.app.exceptions.PlException;

import lombok.Getter;

/**
 * @author sanjay
 *
 */
public class RecordHandler<KeyType, ObjectType> implements IRecordHandler<KeyType, ObjectType>
{
  @Getter
  private Class<?> supportedType;

  protected RecordHandler(Class<?> objType)
  {
    supportedType = objType;
  }

  @Override
  public void createContainer(Connection cxn) throws PlException
  {
    throw new PlException(PLMessages.PL_UNIMPLEMENTED_DB_OPERATION, "createContainer", supportedType.getName());
  }

  @Override
  public void deleteContainer(Connection cxn) throws PlException
  {
    throw new PlException(PLMessages.PL_UNIMPLEMENTED_DB_OPERATION, "deleteContainer", supportedType.getName());
  }

  @Override
  public void create(Connection cxn, ObjectType o) throws PlException
  {
    throw new PlException(PLMessages.PL_UNIMPLEMENTED_DB_OPERATION, "create", supportedType.getName());
  }

  @Override
  public ObjectType read(Connection cxn, KeyType key) throws PlException
  {
    throw new PlException(PLMessages.PL_UNIMPLEMENTED_DB_OPERATION, "read", supportedType.getName());
  }

  @Override
  public void update(Connection cxn, ObjectType o) throws PlException
  {
    throw new PlException(PLMessages.PL_UNIMPLEMENTED_DB_OPERATION, "update", supportedType.getName());
  }

  @Override
  public void delete(Connection cxn, ObjectType o) throws PlException
  {
    throw new PlException(PLMessages.PL_UNIMPLEMENTED_DB_OPERATION, "delete", supportedType.getName());
  }

  @Override
  public StorageResultSet<ObjectType> search(Connection cxn, String condition)
      throws PlException
  {
    throw new PlException(PLMessages.PL_UNIMPLEMENTED_DB_OPERATION, "search", supportedType.getName());
  }
}
