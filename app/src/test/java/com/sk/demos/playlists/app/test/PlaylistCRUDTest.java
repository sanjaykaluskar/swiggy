package com.sk.demos.playlists.app.test;

import java.io.IOException;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import com.sk.demos.playlists.app.drivers.AppMgmt;
import com.sk.demos.playlists.app.exceptions.PlException;
import com.sk.demos.playlists.app.interfaces.IPlaylistMgmt;
import com.sk.demos.playlists.app.svc.PlaylistMgmtFactory;
import com.sk.demos.playlists.app.types.Config;
import com.sk.demos.playlists.app.types.Playlist;
import com.sk.demos.playlists.app.utils.IOUtils;

public class PlaylistCRUDTest
{
  private static final String CONFIG_FILE1 = "src/test/resources/config/config1.txt";

  private static final String DATA_FILE1 = "src/test/resources/data/test_data1.csv";

  @Test
  public void testPlaylistCrud() throws PlException, IOException
  {
    /* read data */
    Config.initFromFile(CONFIG_FILE1);
    List<Playlist> playlists = IOUtils.readPlaylistsFromCsv(DATA_FILE1);
    
    /* set up app */
    AppMgmt.install();

    /* add playlists */
    IPlaylistMgmt plSvc = PlaylistMgmtFactory.getService();
    for (Playlist pl : playlists)
    {
      long id = plSvc.create(pl);
      pl.setId(id);
    }

    /* check additions */
    for (Playlist pl : playlists)
    {
      Playlist o = plSvc.read(pl.getId());
      Assert.assertEquals(pl.getName(), o.getName());
      Assert.assertEquals(pl.getPlays(), o.getPlays());
      Assert.assertEquals(pl.getLikes(), o.getLikes());
    }

    /* check update */
    for (Playlist pl : playlists)
    {
      pl.setPlays(pl.getPlays() + 1);
      plSvc.update(pl);
      Playlist o = plSvc.read(pl.getId());
      Assert.assertEquals(pl.getName(), o.getName());
      Assert.assertEquals(pl.getPlays(), o.getPlays());
    }

    /* check deletions */
    for (Playlist pl : playlists)
    {
      plSvc.delete(pl);
      Assert.assertNull(plSvc.read(pl.getId()));
    }

    /* tear down app */
    AppMgmt.uninstall();
  }
}
