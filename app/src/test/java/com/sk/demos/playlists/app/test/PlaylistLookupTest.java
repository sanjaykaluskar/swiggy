package com.sk.demos.playlists.app.test;

import java.io.IOException;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sk.demos.playlists.app.drivers.AppMgmt;
import com.sk.demos.playlists.app.exceptions.PlException;
import com.sk.demos.playlists.app.interfaces.IPlaylistMgmt;
import com.sk.demos.playlists.app.svc.PlaylistMgmtFactory;
import com.sk.demos.playlists.app.types.Config;
import com.sk.demos.playlists.app.types.Playlist;
import com.sk.demos.playlists.app.utils.IOUtils;

public class PlaylistLookupTest
{
  private static final String CONFIG_FILE1 = "src/test/resources/config/config1.txt";

  private static final String DATA_FILE1 = "src/test/resources/data/test_data1.csv";

  private static Logger logger = LoggerFactory.getLogger(PlaylistLookupTest.class);

  @Test
  public void testPlaylistCrud() throws PlException, IOException
  {
    /* read data */
    Config.initFromFile(CONFIG_FILE1);
    List<Playlist> playlists = IOUtils.readPlaylistsFromCsv(DATA_FILE1);
    
    /* set up app */
    AppMgmt.install();
    logger.trace("Installed app");

    /* add playlists */
    IPlaylistMgmt plSvc = PlaylistMgmtFactory.getService();
    for (Playlist pl : playlists)
    {
      long id = plSvc.create(pl);
      pl.setId(id);
      logger.trace("Created playlist {} with id {}", pl.getName(), String.valueOf(id));
    }

    /* lookup */
    for (Playlist pl : playlists)
    {
      List<String> tags = pl.getTags().getTags();
      List<Playlist> list = plSvc.lookupByTags(tags, null, 1, 5);
      for (Playlist result : list)
      {
        for (String tag : tags)
          Assert.assertTrue(result.containsTag(tag));
      }
    }

    /* tear down app */
    AppMgmt.uninstall();
    logger.trace("Uninstalled app");
  }
}
