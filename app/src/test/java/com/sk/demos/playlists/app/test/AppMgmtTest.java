package com.sk.demos.playlists.app.test;

import java.io.IOException;
import org.junit.Assert;
import org.junit.Test;

import com.sk.demos.playlists.app.drivers.AppMgmt;
import com.sk.demos.playlists.app.exceptions.PlException;
import com.sk.demos.playlists.app.types.Config;

public class AppMgmtTest
{
  private static final String CONFIG_FILE1 = "src/test/resources/config/config1.txt";

  @Test
  public void testInstall() throws PlException, IOException
  {
    Config.initFromFile(CONFIG_FILE1);
    AppMgmt.install();
    AppMgmt.uninstall();
    Assert.assertTrue(true);
  }
}
