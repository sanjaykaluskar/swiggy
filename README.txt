Description:
-----------
This was done as a coding assignment for interview with swiggy.
The goal was to implement a backend API to manage user tags associated with
a playlist. The problem statement is in the document problem-statement.pdf.

-----------------------------------------------------------------------------

Currently the app users Oracle as storage, so an oracle database is needed
to test.

Prerequisites:
-------------
1. Install mvn

2. Install Oracle JDBC driver
   - download ojdbc6.jar from www.oracle.com
   - install it into your local repository
     mvn install:install-file -Dfile=ojdbc6.jar -DgroupId=com.oracle -DartifactId=ojdbc6 -Dversion=11.2.0.XE -Dpackaging=jar

2. Set the right database connection properties in the test files
   (app/src/test/resources/config/config1.txt) to run tests.

Build:
-----
1. Go to root directory
     mvn clean install

   To only build without running tests:
     mvn -DskipTests clean install
